<?php

namespace Drupal\site_health\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

interface SiteHealthCheckConfigInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface{

  /**
   * Gets the Site Health check plugin collection.
   *
   * @return \Drupal\Core\Plugin\DefaultSingleLazyPluginCollection
   */
  public function getCheckCollection();

  /**
   * Gets the Site Health check plugin.
   *
   * @return \Drupal\site_health\Plugin\SiteHealthPluginInterface
   */
  public function getCheck();
}
