<?php

namespace Drupal\site_health\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;
use Drupal\healthcheck\Entity\CheckConfigInterface;

/**
 * Defines the Site Health CheckConfig entity.
 *
 * @ConfigEntityType(
 *   id = "sitehealthcheckconfig",
 *   label = @Translation("Site Health check Plugin Config"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\site_health\Controller\SiteHealthCheckConfigListBuilder",
 *     "form" = {
 *       "edit" = "Drupal\site_health\Form\SiteHealthCheckConfigForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_health\SiteHealthCheckConfigHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "check",
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "healthcheckConfig",
 *   },
 *   admin_permission = "configure site health",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/sitehealth/checks/{sitehealthcheckconfig}/edit",
 *     "collection" = "/admin/config/system/sitehealth/checks"
 *   }
 * )
 */
class SiteHealthCheckConfig extends ConfigEntityBase implements CheckConfigInterface {

  /**
   * The CheckConfig ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The CheckConfig label.
   *
   * @var string
   */
  protected $label;

  /**
   * The site_health check plugin config.
   *
   * @var array
   */
  protected $healthcheckConfig = [];

  /**
   * The healthcheck plugin collection.
   *
   * @var \Drupal\Core\Plugin\DefaultSingleLazyPluginCollection
   */
  protected $checkPluginCollection;

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'healthcheckConfig' => $this->getCheckCollection(),
    ];
  }

  /**
   * Get Health checks.
   * @return DefaultSingleLazyPluginCollection
   *
   */
  public function getCheckCollection() {
    if (!$this->checkPluginCollection) {
      $this->checkPluginCollection = new DefaultSingleLazyPluginCollection(
        \Drupal::service('plugin.manager.sitehealth_plugin'),
        $this->id,
        $this->healthcheckConfig
      );
    }

    return $this->checkPluginCollection;
  }

  public function getCheck() {
    return $this->getCheckCollection()->get($this->id);
  }

  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
  }
}
