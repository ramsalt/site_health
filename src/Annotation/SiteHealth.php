<?php

namespace Drupal\site_health\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 *  Defines a site_health plugin item annotation object.
 *
 *  @see \Drupal\site_health\Plugin\SiteHealthPluginManager
 *
 *  @see plugin_api
 *
 *  @Annotation
 */
class SiteHealth extends Plugin {
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * An array of string tags used to categorize the site health check.
   *
   * @var array
   */
  public $tags;

  /**
   * A description of the check performed.
   *
   * @var string
   */
  public $description;


}
