<?php

namespace Drupal\site_health\Services;
/**
 * Interface SiteHealthHttpServiceInterface.
 */
interface SiteHealthHttpServiceInterface {

  /**
   * Sends the report of checks.
   *
   * @param array $report
   *   The report of checks.
   */
  public function sendReport(array $report);
}
