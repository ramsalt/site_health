<?php

namespace Drupal\site_health\Services;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\site_health\Form\SiteHealthSettingsForm;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class SiteHealthHttpService implements SiteHealthHttpServiceInterface {

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  public function __construct(ClientInterface $client, ConfigFactoryInterface $config_factory) {
    $this->httpClient = $client;
    $this->config = $config_factory->get(SiteHealthSettingsForm::CONF_ID);
  }

  /**
   * {@inheritdoc}
   */
  public function sendReport(array $report) {
    $uri = $this->config->get('base_uri') . '/reports';
    $this->httpClient->request('POST', $uri, [
      'json' => Json::decode($report),
      'auth' => [$this->config->get('username'), $this->config->get('password')],
    ]);
  }
}
