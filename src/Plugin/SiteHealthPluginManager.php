<?php

namespace Drupal\site_health\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides the Site Health plugin manager.
 */
class SiteHealthPluginManager extends DefaultPluginManager {

  use StringTranslationTrait;

  /**
   * Constructs a new Site Health PluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces,
                              CacheBackendInterface $cache_backend,
                              ModuleHandlerInterface $module_handler) {

    parent::__construct('Plugin/SiteHealthCheck', $namespaces, $module_handler, 'Drupal\site_health\Plugin\SiteHealthPluginInterface', 'Drupal\site_health\Annotation\SiteHealth');

    $this->alterInfo('site_health_site_health_plugin_info');
    $this->setCacheBackend($cache_backend, 'site_health_site_health_plugin_plugins');
  }

  /**
   * Gets site health plugin tags as an array.
   *
   * @return array
   *   An array of site health plugin tags.
   */
  public function getTags() {
    $tags = [];

    // Get all the healthcheck plugin definitions.
    $definitions = $this->getDefinitions();

    // Go through each, building the array.
    foreach ($definitions as $definition) {
      foreach ($definition['tags'] as $tag) {
        $tags[$tag] = [
          'label' => $this->t($tag)
        ];
      }
    }

    // Allow other modules to alter and change tag information.
    $this->moduleHandler->alter('sitehealth_tags', $tags);

    return $tags;
  }

  /**
   * Get the list of tags for use in a select list.
   *
   * @return array
   *   An array of tag machine names and display names.
   */
  public function getTagsSelectList() {
    $list = [];

    // Get all the tags.
    $tags = $this->getTags();

    foreach ($tags as $tag_id => $tag) {
      // Use the label if set, otherwise, fallback to the tag's machine name.
      $list[$tag_id] = empty($tag['label']) ? $this->t($tag_id) : $tag['label'];
    }

    return $list;
  }

  /**
   * Get the Healthcheck plugins, filtering by tag and name.
   *
   * @param $tags
   *   Optional. An array of Healthcheck plugin tags.
   * @return array
   *   An array of Healthcheck definitions, keyed by plugin ID.
   */
  public function getDefinitionsByTags($tags = []) {
    $checks = [];

    foreach ($this->getDefinitions() as $name => $definition) {
      $checks[$name] = $definition;
    }

    return $checks;
  }

  /**
   * Get a list of individual checks.
   *
   * @return array
   *   An array of check labels, keyed by plugin ID.
   */
  public function getChecksSelectList() {
    $list = [];
    $defs = $this->getDefinitions();

    foreach ($defs as $def) {
      $id = $def['id'];
      $label = $def['label'];

      $list[$id] = $label;
    }

    return $list;
  }


}
