<?php

namespace Drupal\site_health\Plugin\SiteHealthCheck;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\site_health\Plugin\SiteHealthPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @SiteHealth(
 *  id = "preprocess",
 *  label = @Translation("CSS and JS pre-processing"),
 *   tags = {
 *   "performance",
 *  },
 *  description = "Checks if CSS and JS pre-processing is enabled."
 * )
 */

class PreprocessChecker extends SiteHealthPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * PerformanceChecker constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getChecks() {
    $checks = [];
    $config = $this->configFactory->get('system.performance');

    // Check CSS aggregation.
    $aggregate = $config->get('css.preprocess');
    if (empty($aggregate)) {
      $checks[] = $this->buildCheck('performance', 'preprocess_css', $this->t('CSS aggregation and compression is disabled.'), 'warning');
    }

    // Check JS aggregation.
    $aggregate = $config->get('js.preprocess');
    if (empty($aggregate)) {
      $checks[] = $this->buildCheck('performance', 'preprocess_js', $this->t('JavaScript aggregation and compression is disabled.'), 'warning');
    }

    return $checks;
  }



}
