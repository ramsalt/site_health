<?php

namespace Drupal\site_health\Plugin\SiteHealthCheck;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\site_health\Plugin\SiteHealthPluginBase;
use Drupal\system\SystemManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @SiteHealth(
 * id = "requirements",
 *  label = @Translation("Sites requirements status"),
 *  description = "Checks for site requirements.",
 *  tags = {
 *   "security",
 *  }
 * )
 */
class RequirementsChecker extends SiteHealthPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The system manager.
   *
   * @var \Drupal\system\SystemManager
   */
  protected $systemManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * ModuleStateChecker constructor.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translations service.
   * @param \Drupal\system\SystemManager $system_manager
   *   The system manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SystemManager $system_manager, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->systemManager = $system_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('system.manager'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getChecks() {
    $checks = [];
    // Check run-time requirements and status information.
    $requirements = $this->systemManager->listRequirements();
    foreach ($requirements as $requirement) {
      if (isset($requirement['severity']) && in_array($requirement['severity'], [REQUIREMENT_ERROR, REQUIREMENT_WARNING], TRUE)) {
        $alert_level = $requirement['severity'] === REQUIREMENT_ERROR ? 'error' : 'warning';
        $checks[] = $this->buildCheck('requirement', $this->render($requirement['title']), isset($requirement['value']) ? $this->render($requirement['value']) : '', $alert_level);
      }
    }
    return $checks;
  }

  /**
   * Renders an element.
   *
   * @param string|array $element
   *   The element to be rendered.
   *
   * @return string
   *   A rendered plain text string.
   */
  protected function render($element) {
    if (is_array($element)) {
      return strip_tags((string) $this->renderer->renderPlain($element));
    }
    return strip_tags($element);
  }

}
