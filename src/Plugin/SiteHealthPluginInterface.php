<?php

namespace Drupal\site_health\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

interface SiteHealthPluginInterface extends PluginInspectionInterface, ConfigurableInterface, PluginFormInterface {

  /**
   * Gets the label of site health check.
   *
   * @return string
   */
  public function label();

  /**
   * Gets the tags of the site health check.
   *
   * @return array
   */
  public function getTags();

  /**
   * Gets the description of the site health check.
   *
   * @return string
   */
  public function getDescription();

}
