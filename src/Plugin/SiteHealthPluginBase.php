<?php

namespace Drupal\site_health\Plugin;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class SiteHealthPluginBase.
 */
abstract class SiteHealthPluginBase extends PluginBase implements SiteHealthPluginInterface, ContainerFactoryPluginInterface {

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configuration += $this->defaultConfiguration();
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    foreach (array_keys($this->defaultConfiguration()) as $key) {
      $this->configuration[$key] = $form_state->getValue($key);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Override this.
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $definition = $this->getPluginDefinition();

    return !empty($definition['label']) ? $definition['label'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    $definition = $this->getPluginDefinition();

    return !empty($definition['tags']) ? $definition['tags'] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $definition = $this->getPluginDefinition();

    return !empty($definition['description']) ? $definition['description'] : '';
  }

  /**
   * Build an individual check array.
   *
   * @param string $type
   *   The type of error we're building.
   * @param string $module
   *   The module name.
   * @param string $description
   *   The error description.
   * @param string $alertLevel
   *   The alert level.
   *
   * @return array
   *   A well formatted check.
   */
  public function buildCheck($type, $module, $description, $alertLevel) {
    return [
      'type' => $type,
      'name' => $module,
      'description' => $description,
      'alert_level' => $alertLevel,
    ];
  }


}
