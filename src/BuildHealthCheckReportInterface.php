<?php

namespace Drupal\site_health;

interface BuildHealthCheckReportInterface {

  /**
   * Collects and returns a snapshot of checks.
   *
   * @return array
   *   The array of all checks.
   */
  public function buildReport();


}
