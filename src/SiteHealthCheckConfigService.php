<?php

namespace Drupal\site_health;

use Drupal\site_health\Entity\SiteHealthCheckConfig;
use Drupal\site_health\Plugin\SiteHealthPluginManager;

/**
 * Class SiteHealthCheckConfigService.
 */
class SiteHealthCheckConfigService implements SiteHealthCheckConfigServiceInterface {

  /**
   * Drupal\site_health\Plugin\SiteHealthPluginManager definition.
   *
   * @var \Drupal\site_health\Plugin\SiteHealthPluginManager
   */
  protected $checkPluginMgr;
  /**
   * Constructs a new CheckConfigService object.
   */
  public function __construct(SiteHealthPluginManager $plugin_manager_healthcheck_plugin) {
    $this->checkPluginMgr = $plugin_manager_healthcheck_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function sync() {
    $this->clean();
    $this->createDefaults();
  }

  /**
   * {@inheritdoc}
   */
  public function clean() {
    // Get all the check config objects.
    $check_configs = SiteHealthCheckConfig::loadMultiple();

    // Get all the check plugins.
    $checks = $this->checkPluginMgr->getDefinitions();

    // Remove every item from our list of check configs that has a plugin.
    foreach ($checks as $id => $definition) {
      unset($check_configs[$id]);
    }

    // Delete any check config entities that are left over.
    /** @var \Drupal\site_health\Entity\SiteHealthCheckConfigInterface $check_config */
    foreach ($check_configs as $check_config) {
      $check_config->delete();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createDefaults() {
    // Get all the definitions from the plugin manager.
    $checks = $this->checkPluginMgr->getDefinitions();

    foreach ($checks as $id => $definition) {
      // Try to load the check config.
      $config = SiteHealthCheckConfig::load($id);

      // If not found...
      if (empty($config)) {
        // ..create a new plugin instance...
        $check = $this->checkPluginMgr->createInstance($id, []);

        // ..and the entity to go with it.
        $config = SiteHealthCheckConfig::create([
          'id' => $id,
          'label' => $definition['label'],
          'plugin_config' => $check->getConfiguration(),
        ]);

        $config->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getByTags($tags = []) {
    $checkdefs = $this->checkPluginMgr->getDefinitionsByTags($tags);
    $check_ids = array_keys($checkdefs);

    return SiteHealthCheckConfig::loadMultiple($check_ids);
  }

}
