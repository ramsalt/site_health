<?php

namespace Drupal\site_health;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\site_health\Entity\SiteHealthCheckConfig;
use Drupal\site_health\Entity\SiteHealthCheckConfigInterface;
use Drupal\site_health\Form\SiteHealthSettingsForm;
use Drupal\site_health\Plugin\SiteHealthPluginInterface;
use Drupal\site_health\Plugin\SiteHealthPluginManager;

class BuildHealthCheckReport implements BuildHealthCheckReportInterface {
  /**
   * The dashboard config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Drupal\site_health\Plugin\SiteHealthPluginManager definition.
   *
   * @var \Drupal\site_health\Plugin\SiteHealthPluginManager
   */
  protected $checkPluginMgr;

  /**
   * BuildHealthCheckReport constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, SiteHealthPluginManager $pluginMngr) {
    $this->config = $config_factory->get(SiteHealthSettingsForm::CONF_ID);
    $this->checkPluginMgr = $pluginMngr;
  }

  /**
   * {@inheritdoc}
   */
  public function buildReport() {
    $healthChecks = [];
    $enabledChecks = array_filter($this->config->get('selected_checks'));
    $checks = $this->checkPluginMgr->getDefinitions();

    foreach ($checks as $id => $definition) {
      if (in_array($id, $enabledChecks)) {
        // Try to load the check config.
        $checkConfig = SiteHealthCheckConfig::load($id);
        /** @var \Drupal\site_health\Plugin\SiteHealthPluginInterface $check_plugin */
        $check_plugin = $checkConfig->getCheck();
        $healthChecks[$id] = $check_plugin->getChecks();
      }

    }

    return $report = [
      //'timestamp' => date(\DateTime::ISO8601),
      'username' => $this->config->get('username'),
      'password' => $this->config->get('password'),
      'checks' => $healthChecks,
    ];
  }

}
