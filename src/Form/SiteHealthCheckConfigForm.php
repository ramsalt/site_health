<?php

namespace Drupal\site_health\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;

class SiteHealthCheckConfigForm extends EntityForm {
  use MessengerTrait;
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\site_health\Entity\SiteHealthCheckConfigInterface $checkconfig */
    $checkconfig = $this->entity;

    /** @var \Drupal\site_health\Plugin\SiteHealthPluginInterface $check */
    $check = $checkconfig->getCheck();

    $form = $check->buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    /** @var \Drupal\site_health\Entity\SiteHealthCheckConfigInterface $checkconfig */
    $checkconfig = $this->entity;

    $checkconfig->getCheck()->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\site_health\Entity\SiteHealthCheckConfigInterface $checkconfig */
    $checkconfig = $this->entity;

    $checkconfig->getCheck()->submitConfigurationForm($form, $form_state);

    $status = $checkconfig->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Check.', [
          '%label' => $checkconfig->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Check.', [
          '%label' => $checkconfig->label(),
        ]));
    }
    $form_state->setRedirectUrl($checkconfig->toUrl('collection'));
  }
}
