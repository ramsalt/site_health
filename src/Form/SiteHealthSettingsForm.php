<?php

namespace Drupal\site_health\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_health\Plugin\SiteHealthPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SiteHealthSettingsForm extends ConfigFormBase {
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Healthcheck plugin manager.
   *
   * @var \Drupal\site_health\Plugin\SiteHealthPluginManager
   */
  protected $siteHealthPluginMgr;

  /**
   * The configuration key for SiteHealth settings.
   */
  const CONF_ID = 'site_health.settings';

  /**
   * Constructs a new HealthSettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              SiteHealthPluginManager $check_plugin_mgr) {
    parent::__construct($config_factory);
    $this->configFactory = $config_factory;
    $this->siteHealthPluginMgr = $check_plugin_mgr;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.sitehealth_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONF_ID,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'healthcheck_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get(static::CONF_ID);
    // General configuration.
    $form['#title'] = $this->t('Site Health Connector Settings');
    $form['config'] = [
      '#type' => 'fieldset',
      '#title' => t('Site Health Connector Settings'),
    ];

    $form['config']['enabled'] = [
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Enable to send checks to the Dashboard service.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('enabled'),
    ];
    $form['config']['base_uri'] = [
      '#title' => $this->t('Base URI'),
      '#type' => 'textfield',
      '#default_value' => $config->get('base_uri'),
      '#description' => $this->t('The base URI for the Dashboard API.'),
    ];
    $form['config']['site_uri'] = [
      '#type' => 'url',
      '#title' => $this->t('Provide the site domain/URL'),
      '#description' => $this->t('For example: %url', ['%url' => 'https://yourwebsite.com']),
    ];

    $form['report'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Report configuration')
    ];
    
    $healthCheckOptions = $this->siteHealthPluginMgr->getChecksSelectList();
    $form['report']['selected_checks'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select checks'),
      '#description' => $this->t('Choose specific checks.'),
      '#options' => $healthCheckOptions,
      '#multiple' => TRUE,
      '#default_value' => $config->get('selected_checks'),
    ];

    // Authentication.
    $form['auth'] = [
      '#type' => 'fieldset',
      '#title' => t('RESTAPI Authentication settings'),
    ];
    $form['auth']['username'] = [
      '#title' => $this->t('Username'),
      '#type' => 'textfield',
      '#default_value' => $config->get('username'),
      '#description' => $this->t('The Ramsalt Portal Hosting Dashboard API username for this site.'),
    ];
    $form['auth']['password'] = [
      '#title' => $this->t('Password'),
      '#type' => 'textfield',
      '#default_value' => $config->get('password'),
      '#description' => $this->t('The Ramsalt Portal Hosting Dashboard API password for this site.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(static::CONF_ID)
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('base_uri', $form_state->getValue('base_uri'))
      ->set('site_uri', $form_state->getValue('site_uri'))
      ->set('categories', $form_state->getValue('categories'))
      ->set('username', $form_state->getValue('username'))
      ->set('password', $form_state->getValue('password'))
      ->set('selected_checks', $form_state->getValue('selected_checks'))
      ->save();
  }

}
