<?php

namespace Drupal\site_health\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class SiteHealthCheckConfigListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Check');
    $header['description'] = $this->t('Description');
    $header['tags'] = $this->t('Tags');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\site_health\Entity\SiteHealthCheckConfigInterface $check_config */
    $check_config = $entity;
    $list = array_filter(\Drupal::config('site_health.settings')->get('selected_checks'));

    if (in_array($check_config->get('id'), $list)) {
      $row['label'] = $check_config->label();

      /** @var \Drupal\site_health\Plugin\SiteHealthPluginInterface $check_plugin */
      $check_plugin = $check_config->getCheck();

      $row['description'] = $check_plugin->getDescription();

      $row['tags'] = implode(', ', $check_plugin->getTags());

      return $row + parent::buildRow($entity);
    }

  }
}
