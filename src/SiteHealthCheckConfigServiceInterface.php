<?php

namespace Drupal\site_health;

/**
 * Interface SiteHealthCheckConfigServiceInterface.
 */
interface SiteHealthCheckConfigServiceInterface {

  /**
   * Deletes check config entities with no plugin, creates defaults for those missing.
   */
  public function sync();

  /**
   * Deletes any check config entity which no longer has a valid plugin.
   */
  public function clean();

  /**
   * Creates new check config entities for plugins with no existing entity.
   */
  public function createDefaults();

  /**
   * Get Check Config entities, filtering by tag and name.
   *
   * @param $tags
   *   Optional. An array of Healthcheck plugin tags.
   *
   * @return array
   *   An array of Healthcheck Check Config entities, keyed by plugin ID.
   */
  public function getByTags($tags = []);

}
