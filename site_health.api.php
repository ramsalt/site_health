<?php

/**
 * Alter the information of SiteHealth plugin tags.
 *
 * The $tags parameter is keyed by the tag's "machine name" -- the name used in
 * the 'tags` item in the @SiteHealth  annotation. Here, you can add further
 * information:
 *
 * @code
 * $tags['performance']['label'] = 'Performance & Caching';
 *
 * $tags['performance']['description'] = t('Checks the performance and caching configuration of the site.');
 * @endcode
 *
 * @param $tags
 *   An array of SiteHealth plugin tags, keyed by machine name.
 */
function hook_sitehealth_tags_alter(&$tags) {
}
